    //function to be perform after action
    async function randomNameGenerator() {   
        var selectGender = document.getElementById("gender").value; 
        var selectNationality = document.getElementById("nationality").value;   
        //ajax call to specific resource
    $.ajax({                     
        url: 'https://randomuser.me/api/?' + 'gender=' + selectGender + '&' + 'nat=' + selectNationality,
        success: function (userData) {   
              var randomName = (userData.results[0].name.first + userData.results[0].name.last);
              document.getElementById("resultsFromData").innerHTML = randomName;
              //display object on ui
            console.log(randomName);   
        }
    });
}
