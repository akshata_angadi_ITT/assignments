//POST and GET method call defination

let express = require('express');  
let router = express.Router();  
let fs = require("fs");
let userData = require('./text.json');
let createFolder = require('./createFolders.js');

router.get('/', function (req, res) {  
  fs.readFile("./text.json", 'utf8', function (err, data) {
    console.log( data );
    res.end( data );
 })  
}) 
let bodyParser = require('body-parser');  
let urlencodedParser = bodyParser.urlencoded({ extended: false })  

router.post('/', urlencodedParser, function (req, res) {  
 let response = {  
       name:req.body.name,  
       country:req.body.country,
       state:req.body.state,
       about:req.body.about	   
   }; 
     console.log(response);   
     createFolder(response);
     userData.push(response);
     fs.writeFile('./text.json', JSON.stringify(userData, null, '\t'), function (err) {
      if (err) {
          return console.error(err);
      }
      console.log('Enter data in text.json');
  });
    res.end(JSON.stringify(response, null, '\t')); 
})  
module.exports=router;