//Server for application to execute 

let express = require('express');
let app = express();
let  reqData= require('./apiService');
 
app.use('/api/get',reqData);
app.use('/api/post',reqData);

let server=app.listen(9090,function(){
    console.log("listening to the port http://%s:%s",server.address().address,server.address().port);
});