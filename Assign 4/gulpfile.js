//Gulp js file  to compile typescript 
const gulp = require('gulp');
const webpack = require('webpack');
const webpackStream = require('webpack-stream');
const webpackConfig = require('./webpack.config.js');

gulp.task('js', function (done) {
  gulp.src('./src/app.ts')
    .pipe(webpackStream(webpackConfig, webpack))
    .pipe(gulp.dest('./public'));
  done();
});
gulp.task('default', gulp.parallel('js'));