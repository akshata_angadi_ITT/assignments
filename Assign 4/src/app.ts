//server connection file
export { };
const express = require('express');
const mongoose = require('mongoose');
const url = 'mongodb://localhost/users';
var bodyParser = require('body-parser');

const app = express();

mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
const con = mongoose.connection;

con.on('open', () => {
    console.log('connected to DB...');
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const userRouter = require('./router/users.ts');
app.use('/users', userRouter);


app.listen(9000, () => {
    console.log('User Your server started...');
});

