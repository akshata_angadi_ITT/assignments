//To define class in typescript and exporting to another file
const userInfo = require('./model/user.ts');
class inputData {
    name: string;
    country: string;
    state: string;
    about: string;
    constructor(name: string, country: string, state: string, about: string) {
        this.name = name;
        this.country = country;
        this.state = state;
        this.about = about;
    }

    async postuserDetails(req: any, res: any) {
        let Data = userInfo.findOne({
            name: req.body.name,
            country: req.body.country,
            state: req.body.state
        });
        Data.then(async (info: any) => {
            if (info) {
                res.end("Data already Present in DB");
            }
            else {
                var user = new userInfo({
                    name: req.body.name,
                    country: req.body.country,
                    state: req.body.state,
                    about: req.body.about
                });
                console.log(user);
                try {
                    const data = await user.save();
                    res.end(JSON.stringify(data, null, '\t'));
                } catch (err) {
                    res.send('Error' + err);
                }
            }
        });
    }
};
export = inputData;