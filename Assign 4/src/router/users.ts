const express = require('express');
const router = express.Router();
const userInfo = require('../model/user.ts');
import UserDetails = require("../class");

router.get('/get', async (req: any, res: any) => {
    try {
        const users = await userInfo.find();
        res.end(JSON.stringify(users, null, '\t'));
    } catch (err) {
        res.send('Error' + err);
    }
});

router.post('/post', async (req: any, res: any) => {
    let user = new UserDetails(req.body.name, req.body.country, req.body.state, req.body.about);
    user.postuserDetails(req, res);
});

router.get('/search', async function (req: any, res: any) {
    try {
        let user = await userInfo.find({ name: req.query.name });
        let txt = JSON.stringify(user);
        let obj = JSON.parse(txt);
        let i = 0;
        let result = '';
        while (true) {
            if (obj[i] === undefined) {
                break;
            }
            result += obj[i].name + " from " + obj[i].state + " , " + obj[i].country + " says, " + obj[i].about + " <br> ";
            i++;
        }
        res.send(result);
    } catch (err) {
        res.send("Error" + err);
    }
});
module.exports = router;