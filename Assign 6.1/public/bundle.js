/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ([
/* 0 */
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
var express = __webpack_require__(1);
var mongoose = __webpack_require__(2);
var url = 'mongodb://localhost/users';
var fs = __webpack_require__(3);
var bodyParser = __webpack_require__(4);
var app = express();
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
var con = mongoose.connection;
con.on('open', function () {
    console.log('connected to DB...');
});
app.use(express.static('public'));
app.use(function (req, res, next) {
    var now = new Date();
    var url = req.url;
    var method = req.method;
    var status = res.statusCode;
    var log = "[" + now + "]  " + method + ": " + url + " " + status;
    fs.appendFile('./public/logger.txt', log + "\n", function (error) {
        if (error)
            throw error;
    });
    next();
});
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var userRouter = __webpack_require__(5);
app.use('/users', userRouter);
app.listen(8007, function () {
    console.log('User Your server started...');
});


/***/ }),
/* 1 */
/***/ ((module) => {

module.exports = require('express');

/***/ }),
/* 2 */
/***/ ((module) => {

module.exports = require('mongoose');

/***/ }),
/* 3 */
/***/ ((module) => {

module.exports = require('fs');

/***/ }),
/* 4 */
/***/ ((module) => {

module.exports = require('body-parser');

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
var express = __webpack_require__(1);
var router = express.Router();
var userInfo = __webpack_require__(6);
var input = "";
var logFile = __webpack_require__(7);
var UserDetails = __webpack_require__(8);
router.get('/get', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var users, err_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, userInfo.find()];
            case 1:
                users = _a.sent();
                input = "Data from DB";
                logFile(input);
                res.end(JSON.stringify(users, null, '\t'));
                return [3 /*break*/, 3];
            case 2:
                err_1 = _a.sent();
                res.send('Error' + err_1);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
router.post('/post', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var user;
    return __generator(this, function (_a) {
        user = new UserDetails(req.body.name, req.body.country, req.body.state, req.body.about);
        user.postuserDetails(req, res);
        return [2 /*return*/];
    });
}); });
router.get('/search', function (req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var flag, user, txt, obj, i, result, err_2;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    flag = 0;
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, userInfo.find({ name: req.query.name })];
                case 2:
                    user = _a.sent();
                    txt = JSON.stringify(user);
                    obj = JSON.parse(txt);
                    i = 0;
                    result = '';
                    while (true) {
                        if (obj[i] === undefined) {
                            break;
                        }
                        else {
                            result += obj[i].name + " from " + obj[i].state + " , " + obj[i].country + " says, " + obj[i].about + " <br> ";
                            i++;
                        }
                        flag = 1;
                    }
                    res.send(result);
                    if (flag == 0) {
                        input = "Data Not Present in DB";
                        logFile(input);
                    }
                    else {
                        input = "Data Present in DB";
                        logFile(input);
                    }
                    return [3 /*break*/, 4];
                case 3:
                    err_2 = _a.sent();
                    res.send("Error" + err_2);
                    return [3 /*break*/, 4];
                case 4: return [2 /*return*/];
            }
        });
    });
});
module.exports = router;


/***/ }),
/* 6 */
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


//mongoose schema 
var mongoose = __webpack_require__(2);
var userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    country: {
        type: String,
        required: true,
    },
    state: {
        type: String,
        required: true
    },
    about: {
        type: String,
        required: true
    }
});
module.exports = mongoose.model('user', userSchema);


/***/ }),
/* 7 */
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


var fs = __webpack_require__(3);
function logFile(input) {
    fs.appendFile('./public/logger.txt', input + "\n", function (error) {
        if (error)
            throw error;
    });
}
module.exports = logFile;


/***/ }),
/* 8 */
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {


var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
//To define class in typescript and exporting to another file
var userInfo = __webpack_require__(6);
var input = "";
var logFile = __webpack_require__(7);
var inputData = /** @class */ (function () {
    function inputData(name, country, state, about) {
        this.name = name;
        this.country = country;
        this.state = state;
        this.about = about;
    }
    inputData.prototype.postuserDetails = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var Data;
            var _this = this;
            return __generator(this, function (_a) {
                Data = userInfo.findOne({
                    name: req.body.name,
                    country: req.body.country,
                    state: req.body.state
                });
                Data.then(function (info) { return __awaiter(_this, void 0, void 0, function () {
                    var user, data, err_1;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (!info) return [3 /*break*/, 1];
                                input = "Data already Present in DB";
                                logFile(input);
                                res.end("Data already Present in DB");
                                return [3 /*break*/, 5];
                            case 1:
                                user = new userInfo({
                                    name: req.body.name,
                                    country: req.body.country,
                                    state: req.body.state,
                                    about: req.body.about
                                });
                                console.log(user);
                                _a.label = 2;
                            case 2:
                                _a.trys.push([2, 4, , 5]);
                                return [4 /*yield*/, user.save()];
                            case 3:
                                data = _a.sent();
                                input = "Data is Added to DB";
                                logFile(input);
                                res.end(JSON.stringify(data, null, '\t'));
                                return [3 /*break*/, 5];
                            case 4:
                                err_1 = _a.sent();
                                res.send('Error' + err_1);
                                return [3 /*break*/, 5];
                            case 5: return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    return inputData;
}());
;
module.exports = inputData;


/***/ })
/******/ 	]);
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		if(__webpack_module_cache__[moduleId]) {
/******/ 			return __webpack_module_cache__[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	// startup
/******/ 	// Load entry module
/******/ 	__webpack_require__(0);
/******/ 	// This entry module used 'exports' so it can't be inlined
/******/ })()
;