//server connection file
export { };
const express = require('express');
const mongoose = require('mongoose');
const url = 'mongodb://localhost/users';
const fs = require('fs');
var bodyParser = require('body-parser');

const app = express();

mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
const con = mongoose.connection;

con.on('open', () => {
    console.log('connected to DB...');
});

app.use(express.static('public'));

app.use((req: any, res: any, next: any) => {
    let now = new Date();
    let url = req.url;
    let method = req.method;
    let status = res.statusCode;
    let log = `[${now}]  ${method}: ${url} ${status}`;
    fs.appendFile('./public/logger.txt', log + "\n", (error: any) => {
        if (error) throw error;
    });
    next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const userRouter = require('./router/users.ts');
app.use('/users', userRouter);


app.listen(8007, () => {
    console.log('User Your server started...');
});

