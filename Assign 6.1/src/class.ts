//To define class in typescript and exporting to another file
const userInfo = require('./model/user.ts');
let input = "";
let logFile = require('./router/logFile');
class inputData {
    name: string;
    country: string;
    state: string;
    about: string;
    constructor(name: string, country: string, state: string, about: string) {
        this.name = name;
        this.country = country;
        this.state = state;
        this.about = about;
    }

    async postuserDetails(req: any, res: any) {
        let Data = userInfo.findOne({
            name: req.body.name,
            country: req.body.country,
            state: req.body.state
        });
        Data.then(async (info: any) => {
            if (info) {
                input = "Data already Present in DB";
                logFile(input);
                res.end("Data already Present in DB")
            }
            else {
                var user = new userInfo({
                    name: req.body.name,
                    country: req.body.country,
                    state: req.body.state,
                    about: req.body.about
                });
                console.log(user);
                try {
                    const data = await user.save();
                    input = "Data is Added to DB";
                    logFile(input);
                    res.end(JSON.stringify(data, null, '\t'));
                } catch (err) {
                    res.send('Error' + err);
                }
            }
        });
    }
};
export = inputData;