//mongoose schema 

let mongoose = require('mongoose');

let userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    country: {
        type: String,
        required: true,
    },
    state: {
        type: String,
        required: true

    },
    about: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('user', userSchema);