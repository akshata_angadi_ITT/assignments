const fs = require('fs');

function logFile(log: any) {
    fs.appendFile('./public/logger.txt', log + "\n", (error: any) => {
        if (error) throw error;
    });
}

module.exports = logFile;