  process.on('message', function(m) { 
    console.log('Child process received:', m); 
    process.disconnect();
  });
  function bubbleSort(arr){
    var len = arr.length;
    for (var i = len-1; i>=0; i--){
      for(var j = 1; j<=i; j++){
        if(arr[j-1]>arr[j]){
            var temp = arr[j-1];
            arr[j-1] = arr[j];
            arr[j] = temp;
         }
      }
    }
    console.log(arr);
 }
 console.log("Array before Sorting:");
 arr=[7,5,2,4,3,9];
 console.log(arr);
 console.log("Array After Sorting:");
 bubbleSort(arr);
process.send({ hello: 'from child process' }); 
  
     
