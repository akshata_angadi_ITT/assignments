//server connection file
const express = require('express');
const fs = require('fs');
const userRouter = require('./worker');
var bodyParser = require('body-parser');
const { Worker, ismainThread } = require('worker_threads');
const swaggerUi = require("swagger-ui-express");
const app = express();
const swaggerDocs = require('./swagger.json');

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/users', userRouter);

if (ismainThread) {

  console.log("I am in Main Thread");

  const worker = new Worker('./src/worker.js');

  worker.on('message', (message) => {
    console.log(message);
  });

  worker.on('exit', (code) => {
    console.log(code)
  });
}

app.use(express.static('public'));
app.use((req, res, next) => {
  let now = new Date();
  let url = req.url;
  let method = req.method;
  let status = res.statusCode;
  let log = `[${now}]  ${method}: ${url} ${status}`;
  fs.appendFile('./public/logger.txt', log + "\n", (error) => {
    if (error) throw error;
  });
  next();
});

app.listen(3001, () => {
  console.log('User Your server started...');
});