let mongoose = require('mongoose');
const url = `mongodb://localhost/users`;

function mongooseConncetion() {
    mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
    let connectionHolder = mongoose.connection;

    connectionHolder.on('open', function () {
        console.log("Connected to database successfully");
    })
}
module.exports = mongooseConncetion;