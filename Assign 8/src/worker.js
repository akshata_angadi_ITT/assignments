//worker thread for get, post and search method
const { ismainThread, parentPort } = require('worker_threads');
const express = require('express');
const router = express.Router();
const mongooseconnection = require('./mongooseconnection');
let userDetails = require('./class');
const userInfo = require('./model/user');
let input = "";
let result;
let logFile = require('./router/logFile');

if (!ismainThread) {
    mongooseconnection();
    router.get('/get', async (req, res) => {
        try {
            const users = await userInfo.find();
            result = res.end(JSON.stringify(users, null, '\t'));
        } catch (err) {
            res.send('Error' + err);
        }
        console.log("GET Method is Done ....");

    });

    router.post('/post', async (req, res) => {
        let user = new userDetails(req.body.name, req.body.country, req.body.state, req.body.about);
        user.postuserDetails(req, res);
        console.log("POST Method is Done ....");
    });

    router.get('/search', async function (req, res) {
        let flag = 0;
        try {
            let user = await userInfo.find({ name: req.query.name });
            let txt = JSON.stringify(user);
            let obj = JSON.parse(txt);
            let i = 0;
            let result = '';
            while (true) {
                if (obj[i] === undefined) {
                    break;
                }
                else {
                    result += obj[i].name + " from " + obj[i].state + " , " + obj[i].country + " says, " + obj[i].about + " <br> ";
                    i++;
                }
                flag = 1;
            }
            res.send(result);
            if (flag == 0) {
                input = "Data Not Present in DB"
                logFile(input);
            }
            else {
                input = "Data Present in DB"
                logFile(input);
            }
        } catch (err) {
            res.send("Error" + err);
        }
        console.log("Search Method is Done ....");
    });
}
module.exports = router;