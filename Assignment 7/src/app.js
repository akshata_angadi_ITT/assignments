//server connection file
const express = require('express');
const mongoose = require('mongoose');
const url = 'mongodb://localhost/users';
const fs = require('fs');
const userRouter = require('./worker');
var bodyParser = require('body-parser');
const { Worker, ismainThread } = require('worker_threads');
const app = express();

mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
const con = mongoose.connection;

con.on('open', () => {
  console.log('connected to Data Base...');
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/users', userRouter);

if (ismainThread) {

  console.log("I am in Main Thread");

  const worker = new Worker('./src/worker.js');

  worker.on('message', (message) => {
    console.log(message);
  });

  worker.on('exit', (code) => {
    console.log(code)
  });
}

app.use(express.static('public'));
app.use((req, res, next) => {
  let now = new Date();
  let url = req.url;
  let method = req.method;
  let status = res.statusCode;
  let log = `[${now}]  ${method}: ${url} ${status}`;
  fs.appendFile('./public/logger.txt', log + "\n", (error) => {
    if (error) throw error;
  });
  next();
});

app.listen(3001, () => {
  console.log('User Your server started...');
});