//To define class in typescript and exporting to another file
const userInfo = require('./model/user');
let input = "";
let logFile = require('./router/logFile');
class inputData {
    async postuserDetails(req, res) {
        let Data = userInfo.findOne({
            name: req.body.name,
            country: req.body.country,
            state: req.body.state
        });
        Data.then(async (info) => {
            if (info) {
                input = "Data already Present in DB";
                logFile(input);
                res.end("Data already Present in DB")
            }
            else {
                var user = new userInfo({
                    name: req.body.name,
                    country: req.body.country,
                    state: req.body.state,
                    about: req.body.about
                });
                try {
                    const data = await user.save();
                    input = "Data is Added to DB";
                    logFile(input);
                    res.end(JSON.stringify(data, null, '\t'));
                } catch (err) {
                    res.send('Error' + err);
                }
            }
        });
    }
};
module.exports = inputData;