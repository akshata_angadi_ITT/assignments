const fs = require('fs');

function logFile(log) {
    fs.appendFile('./public/logger.txt', log + "\n", (error) => {
        if (error) throw error;
    });
}

module.exports = logFile;