 //function defination for application
 async function randomNameGenerator(){      
      var genderOfData : string =((document.getElementById("Gender") as HTMLInputElement).value);              //getting user gender input from ui
      var nationalityOfData :string = ((document.getElementById("Nationality") as HTMLInputElement).value);    //getting user nationality input from ui
//ajax method 
     $.ajax({ 
        url: 'https://randomuser.me/api/?' + 'gender=' + genderOfData + '&' + 'nat=' + nationalityOfData,   //api call
        success: function (userData) {       //return data after ajax call success  
            var userInfo = (userData.results[0].name.first + userData.results[0].name.last);
            document.getElementById("resultsOfData").innerHTML = userInfo;
            console.log(userInfo);     //display object on ui
        }
    });
}
