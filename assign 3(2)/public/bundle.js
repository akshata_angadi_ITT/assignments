/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ([
/* 0 */,
/* 1 */
/***/ ((module) => {

module.exports = require('express');

/***/ }),
/* 2 */
/***/ ((module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
//GET and POST methods call Defination
let express = __webpack_require__(1);
let router = express.Router();
let fs = __webpack_require__(3);
let createFolder = __webpack_require__(4);
let bodyParser = __webpack_require__(5);
const Data = __webpack_require__(6);
router.get('/', function (req, res) {
    fs.readFile("./text.json", 'utf8', function (err, data) {
        console.log(data);
        res.end(data);
    });
});
let urlencodedParser = bodyParser.urlencoded({ extended: false });
router.post('/', urlencodedParser, function (req, res) {
    let obj = new Data(req.body.name, req.body.country, req.body.state, req.body.about);
    let userData = obj.userDetails(req, res);
    console.log(userData);
    createFolder(userData);
    res.end(JSON.stringify(userData, null, '\t'));
});
module.exports = router;


/***/ }),
/* 3 */
/***/ ((module) => {

module.exports = require('fs');

/***/ }),
/* 4 */
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


//To create folders for user input
let fs = __webpack_require__(3);
let createFolder = function (userData) {
    if (!fs.existsSync('./' + userData.country)) {
        fs.mkdirSync('./' + userData.country);
    }
    if (!fs.existsSync('./' + userData.country + '/' + userData.state)) {
        fs.mkdirSync('./' + userData.country + '/' + userData.state);
    }
    fs.mkdirSync('./' + userData.country + '/' + userData.state + '/' + userData.name);
    let aboutOfUserInput = userData.about;
    let directoryOfUserInput = './' + userData.country + '/' + userData.state + '/' + userData.name;
    //write user entered about in text file
    fs.writeFile(`${directoryOfUserInput}/about.txt`, aboutOfUserInput, function (err) {
        if (err)
            throw err;
    });
};
module.exports = createFolder;


/***/ }),
/* 5 */
/***/ ((module) => {

module.exports = require('body-parser');

/***/ }),
/* 6 */
/***/ ((module) => {


//To define class in typescript and exporting to another file
class Data {
    constructor(name, country, state, about) {
        this.name = name;
        this.country = country;
        this.state = state;
        this.about = about;
    }
    userDetails(req, res) {
        let response = {
            name: this.name,
            country: this.country,
            state: this.state,
            about: this.about
        };
        return response;
    }
}
;
module.exports = Data;


/***/ })
/******/ 	]);
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		if(__webpack_module_cache__[moduleId]) {
/******/ 			return __webpack_module_cache__[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
(() => {

//To create server to run Appliation 
let express = __webpack_require__(1);
let app = express();
let reqData = __webpack_require__(2);
app.use('/api/get', reqData);
app.use('/api/post', reqData);
let server = app.listen(3000, function () {
    console.log("listening to the port http://%s:%s", server.address().address, server.address().port);
});

})();

/******/ })()
;