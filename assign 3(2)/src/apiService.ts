//GET and POST methods call Defination
let express = require('express');
let router = express.Router();
let fs = require('fs');
let createFolder = require('./createFolders.ts');
let bodyParser = require('body-parser');
import Data = require('./classOfData');

router.get('/', function (req: any, res: any) {
   fs.readFile("./text.json", 'utf8', function (err: any, data: any) {
      console.log(data);
      res.end(data);
   })
})

let urlencodedParser = bodyParser.urlencoded({ extended: false })
router.post('/', urlencodedParser, function (req: any, res: any) {
   let obj = new Data(req.body.name, req.body.country, req.body.state, req.body.about);
   let userData = obj.userDetails(req, res);
   console.log(userData);
   createFolder(userData);
   res.end(JSON.stringify(userData, null, '\t'));
})
module.exports = router;