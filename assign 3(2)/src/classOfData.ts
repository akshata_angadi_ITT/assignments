//To define class in typescript and exporting to another file
class Data {
	name: string;
	country: string;
	state: string;
	about: string;
	constructor(name: string, country: string, state: string, about: string) {
		this.name = name;
		this.country = country;
		this.state = state;
		this.about = about;
	}

	userDetails(req: any, res: any) {
		let response = {
			name: this.name,
			country: this.country,
			state: this.state,
			about: this.about
		};
		return response;
	}
};
export = Data;