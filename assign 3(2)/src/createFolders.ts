//To create folders for user input
let fs = require('fs');
let createFolder = function (userData: any) {
  if (!fs.existsSync('./' + userData.country)) {
    fs.mkdirSync('./' + userData.country);
  }
  if (!fs.existsSync('./' + userData.country + '/' + userData.state)) {
    fs.mkdirSync('./' + userData.country + '/' + userData.state);
  }
  fs.mkdirSync('./' + userData.country + '/' + userData.state + '/' + userData.name);

  let aboutOfUserInput = userData.about;
  let directoryOfUserInput = './' + userData.country + '/' + userData.state + '/' + userData.name;

  //write user entered about in text file
  fs.writeFile(`${directoryOfUserInput}/about.txt`, aboutOfUserInput, function (err: any) {
    if (err)
      throw err;
  });
}
module.exports = createFolder;