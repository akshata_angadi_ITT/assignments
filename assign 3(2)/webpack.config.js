const path=require('path');
//Gulp js file  to compile typescript 
module.exports={
	mode : "none",
	entry:'./src/mainServer.ts',
	module:{
		rules:[
		{
			test:/\.ts$/,
			use:'ts-loader',
			include:[path.resolve(__dirname,'src')]
		}
	]
	},
	resolve:{
		extensions:['.ts','.js']
	},
	output:{
		publicPath:'public',
		filename:'bundle.js',
		path:path.resolve(__dirname,'public')
	},
  "externals": {
  
     "fs": "require('fs')",
     "express" : "require('express')",
     "url" : "require('url')",
     "body-parser": "require('body-parser')"
   
  }

}